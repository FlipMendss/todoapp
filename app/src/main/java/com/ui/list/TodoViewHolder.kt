package com.ui.list

import android.view.View
import android.widget.CheckBox
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.model.Todo

class TodoViewHolder (
    itemView: View,
    private val adapter: TodoAdapter
): RecyclerView.ViewHolder(itemView){


    private val frameUrgent: FrameLayout = itemView.findViewById(R.id.frame_color)
    private val txtDescription: TextView = itemView.findViewById(R.id.txtDescription)
    private val chxDone: CheckBox = itemView.findViewById(R.id.chxDone)
    private lateinit var currentTask: Todo

    init{
            itemView.setOnClickListener{
                this.adapter.getOnClickTaskListener()?.onClickTask(this.currentTask)
            }
            itemView.setOnLongClickListener{
                this.adapter.getOnLongClickTaskListener()?.onLongClickTask(this.currentTask)
                true
            }
            this.chxDone.setOnCheckedChangeListener{ _, b->
                this.adapter.getOnCheckBoxClickTaskListener()?.onCheckBoxClickTask(this.currentTask,b)

            }

    }

    fun bind(todo: Todo) {
        this.currentTask = todo
        this.txtDescription.text = this.currentTask.description
        this.chxDone.isChecked = this.currentTask.isDone
        if(this.currentTask.isUrgent){
            this.frameUrgent.setBackgroundColor(ContextCompat.getColor(itemView.context,R.color.red))
        }else{
            this.frameUrgent.setBackgroundColor(ContextCompat.getColor(itemView.context,R.color.green))
        }

    }
}