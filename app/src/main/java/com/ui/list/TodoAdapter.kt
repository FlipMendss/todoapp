package com.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.model.Todo

class TodoAdapter(
    private var tasks:ArrayList<Todo>
): RecyclerView.Adapter<TodoViewHolder>(){
    private var listener: OnClickTaskListener? = null
    private var longListener: OnLongClickTaskListener? = null
    private var chxListener: OnCheckBoxClickTaskListener? = null

    fun interface OnClickTaskListener{
        fun onClickTask(todo:Todo)
    }
    fun interface OnLongClickTaskListener{
        fun onLongClickTask(todo:Todo)
    }

    fun interface OnCheckBoxClickTaskListener{
        fun onCheckBoxClickTask(todo: Todo, b: Boolean)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val layoutRes = R.layout.todo
        val itemView = layoutInflater.inflate(layoutRes, parent, false)
        return TodoViewHolder(itemView, this)
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        holder.bind(this.tasks[position])
    }

    override fun getItemCount(): Int {
        return this.tasks.size
    }

    fun setOnClickTaskListener(listener: OnClickTaskListener?) {
        this.listener = listener
    }

    fun getOnClickTaskListener(): OnClickTaskListener? {
        return this.listener
    }

    fun setOnLongClickTaskListener(longListener: OnLongClickTaskListener?) {
        this.longListener = longListener
    }

    fun getOnLongClickTaskListener(): OnLongClickTaskListener? {
        return this.longListener
    }
    fun setOnCheckBoxClickTaskListener(chxListener: OnCheckBoxClickTaskListener?) {
        this.chxListener = chxListener
    }

    fun getOnCheckBoxClickTaskListener(): OnCheckBoxClickTaskListener? {
        return this.chxListener
    }



}

