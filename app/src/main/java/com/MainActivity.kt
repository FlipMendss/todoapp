package com

import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todoapp.R
import com.model.Todo
import com.ui.list.TodoAdapter

class MainActivity:AppCompatActivity() {
    private lateinit var etxtTodo: EditText
    private lateinit var tasks: ArrayList<Todo>
    private lateinit var rvTasks: RecyclerView
    private lateinit var todoAdapter: TodoAdapter
    private lateinit var switchUrgent: Switch
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.etxtTodo = findViewById(R.id.etxtTodo)
        this.rvTasks = findViewById(R.id.rvTasks)
        this.switchUrgent = findViewById(R.id.urgent_switch)
        if(savedInstanceState == null){
            this.tasks = ArrayList()
        } else if (savedInstanceState.containsKey("RECOVER_TASKS")){
            this.tasks =
                        savedInstanceState.getParcelableArrayList<Todo>("RECOVER_TASKS")
                        as ArrayList<Todo>
        }
        this.todoAdapter = TodoAdapter(this.tasks)

        this.todoAdapter.setOnCheckBoxClickTaskListener {todo, boolean ->
            todo.isDone = boolean
        }

        this.todoAdapter.setOnClickTaskListener{todo ->
            todo.isDone = !todo.isDone
            todoAdapter.notifyItemChanged(this.tasks.indexOf(todo))

        }

        this.todoAdapter.setOnLongClickTaskListener{todo ->
            this.tasks.remove(todo)
            Toast.makeText(this, "Tarefa \""+ todo.description +"\" apagada com sucesso!!", Toast.LENGTH_SHORT).show()
            todoAdapter.notifyItemRemoved(this.tasks.indexOf(todo))
        }

        this.rvTasks.layoutManager = LinearLayoutManager(this)
        this.rvTasks.adapter = this.todoAdapter

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList("RECOVER_TASKS", this.tasks)
    }

    fun onClickSend(v: View){
        val todo = this.etxtTodo.text.toString()
        if(todo.isNotBlank()){
            val todoToInclude = Todo(
                this.switchUrgent.isChecked,
                todo,
                false
            )
            this.tasks.add(todoToInclude)
            this.todoAdapter.notifyItemInserted(this.tasks.size-1)
            this.rvTasks.scrollToPosition(this.tasks.size-1)
            this.switchUrgent.isChecked = false
            this.etxtTodo.text.clear()
        }
    }
}