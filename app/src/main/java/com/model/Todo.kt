package com.model

import android.os.Parcel
import android.os.Parcelable

class Todo (
    val isUrgent: Boolean,
    val description: String,
    var isDone: Boolean
    ): Parcelable{
        constructor(parcel: Parcel):this(
        parcel.readByte() !=0.toByte(),
        parcel.readString()!!,
        parcel.readByte() != 0.toByte()
        ) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeByte(if (isUrgent) 1 else 0)
            parcel.writeString(description)
            parcel.writeByte(if (isDone) 1 else 0)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<Todo> {
            override fun createFromParcel(parcel: Parcel): Todo {
                return Todo(parcel)
            }

            override fun newArray(size: Int): Array<Todo?> {
                return arrayOfNulls(size)
            }
        }


    }
